{{define "resources"}}
          requests:
            cpu: {{.Values.resources.req.cpu}}
            memory: {{.Values.resources.req.memory}}
          limits:
            cpu: {{.Values.resources.limits.cpu}}
            memory: {{.Values.resources.limits.memory}}
{{end}}