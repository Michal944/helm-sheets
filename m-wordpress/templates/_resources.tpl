{{define "resources-mysql"}}
          requests:
            memory: "128Mi"
            cpu: "200m"
          limits:
            memory: "256Mi"
            cpu: "400m"
{{end}}

{{define "resources-wp"}}
          requests:
            cpu: "200m"
            memory: "128Mi"
          limits:
            memory: "756Mi"
            cpu: "600m"
{{end}}